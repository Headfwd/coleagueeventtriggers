import React from 'react';

import { TriggerScreen } from './src/components/screens';

console.disableYellowBox = true;

const App = () => <TriggerScreen />

export default App;
