import React from 'react';

import Triggers from '../../modules/Triggers';

import { Container, Header, Content } from './styled';

const TriggerScreen = () => (
  <Container>
    <Header>CO-LEAGUE TRIGGERS</Header>
    <Content>
      <Triggers />
    </Content>
  </Container>
);

export default TriggerScreen;
