import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  padding: 60px 40px;

`;

export const Header = styled.Text`
  font-size: 42px;
  margin-bottom: 24px;
`;

export const Content = styled.View`
  flex: 1;
`;
