import React from 'react';

import { Container } from './styled';
import { TriggerSelector, TriggerForm, TriggerSubmit } from './components';

export interface Parameter {
  label: string;
  keyboard: 'number-pad' | 'default';
  type: 'number' | 'string';
};

export interface Trigger {
  id: number;
  label: string;
  path: string;
  parameters?: Parameter[];
  body?: {};
};

const triggers = [
  {
    id: 1,
    label: 'Countdown',
    path: '/countdown',
    parameters: [{
      label: 'seconds',
      keyboard: 'number-pad',
      type: 'number'
    }],
  },
  {
    id: 2,
    label: 'Refresh',
    path: '/refresh',
  },
  {
    id: 3,
    label: 'Switch stage',
    path: '/switchStage',
    parameters: [{
      label: 'switchTo',
      keyboard: 'default',
      type: 'string',
    }]
  }
];

const TriggerContext = React.createContext(null);

export const useTriggers = () => {
  const context = React.useContext(TriggerContext);

  if (!context) throw new Error('Trigger components should be rendered inside the Trigger component');

  return context;
}

const Triggers = () => {
  const [activeTrigger, setActiveTrigger] = React.useState<Trigger>(triggers[0]);
  const [body, setBody] = React.useState<{}>({});

  const switchTrigger = React.useCallback((trigger: Trigger) => {
    setBody({});
    setActiveTrigger(trigger);
  }, []);

  const value = React.useMemo(() => ({
    setBody, body, switchTrigger, triggers, activeTrigger,
  }), [setBody, body, switchTrigger, triggers, activeTrigger]);

  return (
    <TriggerContext.Provider value={value}>
      <Container>
        <TriggerSelector />
        <TriggerForm />
      </Container>
      <TriggerSubmit />
    </TriggerContext.Provider>
  );
};

export default Triggers;
