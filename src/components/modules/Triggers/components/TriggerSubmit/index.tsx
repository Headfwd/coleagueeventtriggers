import React from 'react';
import { Alert } from 'react-native';

import { useTriggers, Trigger } from '../../';
import { Container, Button, Label } from './styled';

const TriggerSubmit = () => {
  const [isRequestion, setIsRequesting] = React.useState(false);
  const { activeTrigger, body } = useTriggers();

  const triggerEvent = React.useCallback(() => {
    fetch(`https://fifa-backend.headfwd.com${activeTrigger.path}`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
        'secret': 'v[)BGf^#Y7)9;41J]Abn1fWE|(W[|eTQz}gW/C6Sz6Bf|Od4O^Szg^@k&Ba-PIx',
      }
    }).then((response) => {
      if (!response.ok) throw new Error('Failure');

      return response.json();
    }).then((json) => {
      if (!json) throw new Error('Failure');
      Alert.alert('Success');
    }).catch(() => {
      Alert.alert('Failure');
    }).finally(() => setIsRequesting(false));
  }, [activeTrigger, body]);

  const confirm = () => {
    setIsRequesting(true);

    Alert.alert(
      `Are you sure you want to trigger an ${activeTrigger.label} event?`,
      '',
      [
        {
          text: 'No',
          onPress: () => setIsRequesting(false),
          style: 'cancel',
        },
        {
          text: 'Yes',
          onPress: triggerEvent,
        },
      ]
    );
  }

  return (
    <Container>
      <Button
        onPress={confirm}
        disabled={!activeTrigger || isRequestion}
      >
        <Label>trigger</Label>
      </Button>
    </Container>
  )
};

export default TriggerSubmit;
