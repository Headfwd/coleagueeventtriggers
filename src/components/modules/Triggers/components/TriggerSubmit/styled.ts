import styled, { css } from 'styled-components/native';

export const Container = styled.View`
  justify-content: center;
  align-items: center;
`;

export const Button = styled.TouchableOpacity`
  background-color: black;
  border-radius: 8px;
  padding: 8px 12px;

  ${({ disabled }) => disabled && css`
    background-color: grey;
  `}
`;

export const Label = styled.Text`
  color: white;
  font-size: 18px;
`;
