import React from 'react';
import { StyleSheet } from 'react-native';

import RNPickerSelect from 'react-native-picker-select';

import { Container } from './styled';
import { useTriggers, Trigger } from '../../';

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

const TriggerSelector = () => {
  const { triggers, switchTrigger, activeTrigger } = useTriggers();

  return (
    <Container>
      <RNPickerSelect
        placeholder={{ label: 'Select an item', value: null }}
        onValueChange={(value) => switchTrigger(triggers.find((trigger: Trigger) => trigger.id === value))}
        items={triggers.map((trigger: Trigger) => ({
          label: trigger.label,
          value: trigger.id,
        }))}
        value={activeTrigger ? activeTrigger.id : null}
        style={pickerSelectStyles}
      />
    </Container>
  );
};

export default TriggerSelector;
