import React from 'react';

import { Container, StyledInput, Item, Label } from './styled';
import { useTriggers, Parameter } from '../../';

const TriggerForm = () => {
  const { activeTrigger, setBody, body } = useTriggers();

  const updateValue = (value: string, parameter: Parameter) => {
    const newBody = {
      ...body,
    };

    newBody[parameter.label] = parameter.type === 'number' ? parseInt(value, 10) : value;

    setBody(newBody);
  };

  if (!activeTrigger) return null;

  return (
    <Container>
      {!!activeTrigger.parameters && activeTrigger.parameters.map((parameter: Parameter) => (
        <Item key={parameter.label}>
          <Label>{parameter.label}</Label>
          <StyledInput
            onChangeText={(text: string) => updateValue(text, parameter)}
            keyboardType={parameter.keyboard}
            autoCapitalize="none"
            returnKeyType='done'
          />
        </Item>
      ))}
    </Container>
  )
};

export default TriggerForm;
