import styled from 'styled-components/native';

export const Container = styled.View`
  margin-top: 40px;
`;

export const StyledInput = styled.TextInput`
  border: 1px solid gray;
  border-radius: 8px;
  padding: 10px 8px;
`;

export const Item = styled.View`

`;

export const Label =  styled.Text`

`;
